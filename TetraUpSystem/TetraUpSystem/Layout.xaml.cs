﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TetraUpSystem
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Layout : ContentView
	{
        #region 変数
        /// <summary>
        /// メニューのレイアウト情報を保持します。
        /// </summary>
        private Dictionary<MenuItem, StackLayout> m_MenuLayout;

        /// <summary>
        /// 現在表示中のボタン
        /// </summary>
        private MenuItem m_MenuItemState;
        #endregion

        #region コンストラクタ
        /// <summary>
        /// コンストラクト
        /// </summary>
        /// 
        public Layout()
        {
            try
            {
                var grid = new Grid
                {
                    HorizontalOptions = LayoutOptions.FillAndExpand,
                    VerticalOptions = LayoutOptions.Center,
                    RowDefinitions =
                {
                    new RowDefinition(){Height = GridLength.Auto}
                },
                    ColumnDefinitions =
                {
                    new ColumnDefinition(){Width = new GridLength(1,GridUnitType.Star)},
                    new ColumnDefinition(){Width = new GridLength(1,GridUnitType.Star)},
                    new ColumnDefinition(){Width = new GridLength(1,GridUnitType.Star)},
                    new ColumnDefinition(){Width = new GridLength(1,GridUnitType.Star)},
                    new ColumnDefinition(){Width = new GridLength(1,GridUnitType.Star)},
                }
                };
                //メニューのレイアウト
                m_MenuLayout = new Dictionary<MenuItem, StackLayout>();

                StackLayout layout = null;
                layout = GetLayout("icon_Home.png", "ホーム");
                grid.Children.Add(layout, 0, 0);
                m_MenuLayout.Add(MenuItem.Home, layout);

                layout = GetLayout("icon_Home.png", "カレンダー");
                grid.Children.Add(layout, 1, 0);
                m_MenuLayout.Add(MenuItem.Calender, layout);

                layout = GetLayout("icon_Home.png", "スクール");
                grid.Children.Add(layout, 2, 0);
                m_MenuLayout.Add(MenuItem.School, layout);

                layout = GetLayout("icon_Home.png", "トーク");
                grid.Children.Add(layout, 3, 0);
                m_MenuLayout.Add(MenuItem.Talk, layout);

                layout = GetLayout("icon_Home.png", "プロフィール");
                grid.Children.Add(layout, 4, 0);
                m_MenuLayout.Add(MenuItem.Profile, layout);

                grid.BackgroundColor = Color.FromHex("F3F3F3");
                grid.Padding = new Thickness(0, 10, 0, 10);

                Content = grid;
                m_MenuItemState = MenuItem.Home;
            }
            catch (Exception)
            {
                Console.WriteLine("例外発生");
            }
        }
        #endregion

        #region メソッド
        /// <summary>
        /// レイアウトを作成します。
        /// </summary>
        /// <param name="sImagePath">画像パス</param>
        /// <param name="sLblName"></param>
        /// <returns>レイアウト</returns>

        private StackLayout GetLayout(string sImagePath, string sLblName)
        {
            Image img = new Image();
            img.Source = ImageSource.FromResource(sImagePath);
            img.WidthRequest = 32;
            img.HeightRequest = 32;
            img.ClassId = sImagePath;

            Label lbl = new Label();
            lbl.Text = sLblName;
            lbl.HorizontalOptions = LayoutOptions.Center;
            lbl.FontSize = 8;
            lbl.TextColor = Color.FromHex("4B4B4D");

            StackLayout stack = new StackLayout();
            stack.VerticalOptions = LayoutOptions.Center;
            stack.HorizontalOptions = LayoutOptions.FillAndExpand;
            stack.Children.Add(img);
            stack.Children.Add(lbl);
            stack.ClassId = "off";

            var tap = new TapGestureRecognizer();
            tap.Tapped += (s, e) =>
            {
                MenuTap((StackLayout)s);
            };
            stack.GestureRecognizers.Add(tap);

            return stack;
        }
        /// <summary>
        /// メニュータップ時の処理をします。
        /// </summary>
        /// <param name="stack">レイアウト</param>

        private void MenuTap(StackLayout stack)
        {
            int nCount = 0;
            Image img = null;
            //メニューをoffにします
            OffMenu();

            foreach (var layout in stack.Children)
            {
                if (nCount == 0)
                {
                    img = (Image)layout;
                }
                if (nCount == 1)
                {
                    Label lbl = (Label)layout;
                    lbl.TextColor = Color.Blue;
                    switch (lbl.Text)
                    {
                        case "ホーム":
                            m_MenuItemState = MenuItem.Home;
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Home.png");
                            break;
                        case "プロジェクト":
                            m_MenuItemState = MenuItem.Calender;
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Calender.png");
                            break;
                        case "スクール":
                            m_MenuItemState = MenuItem.School;
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_School.png");
                            break;
                        case "トーク":
                            m_MenuItemState = MenuItem.Talk;
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Talk.png");
                            break;
                        case "プロフィール":
                            m_MenuItemState = MenuItem.Profile;
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Profile.png");
                            break;
                    }
                }
                nCount++;
            }
            //MainPage.MoveMenu(m_MenuItemState);
        }

        /// <summary>
        /// メニューボタンをoffにします。
        /// </summary>
        private void OffMenu()
        {
            StackLayout stack = m_MenuLayout[m_MenuItemState];
            int nCount = 0;
            Image img = null;

            foreach (var layout in stack.Children)
            {
                if (nCount == 0)
                {
                    img = (Image)layout;
                }
                if (nCount == 1)
                {
                    Label lbl = (Label)layout;
                    lbl.TextColor = Color.FromHex("4B4B4D");
                    switch (lbl.Text)
                    {
                        case "ホーム":
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Home.png");
                            break;
                        case "プロジェクト":
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Calender.png");
                            break;
                        case "オファー":
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Detail.png");
                            break;
                        case "トーク":
                            img.Source = ImageSource.FromResource("TetraUpSystem.Image.icon_Talk.png");
                            break;
                    }
                }
                nCount++;
            }
        }

        #endregion

    }
}