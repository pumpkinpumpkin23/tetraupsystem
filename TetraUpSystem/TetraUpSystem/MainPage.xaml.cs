﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Xamarin.Forms;

namespace TetraUpSystem
{
    public enum MenuItem
    {
        Home,
        Calender,
        School,
        Talk,
        Profile,
    }
    public partial class MainPage : ContentPage
	{
        #region メンバ変数
        private StackLayout m_Main;

        private ContentView m_Home;
        private ContentView m_Calender;
        private ContentView m_School;
        private ContentView m_Talk;
        private ContentView m_Profile;

        private StackLayout m_Menu;
        private ContentView m_Layout;
        private ContentView m_MyPage;

        public int n = 1;

        #endregion

        #region コンストラクタ

        public MainPage()
		{
            InitializeComponent();
            CreateLayout();
        }
        #endregion


        #region メソッド
        /// <summary>
        /// メニューを移動します
        /// </summary>
 
        public void MoveMenu(MenuItem menuItem)
        {
            var x = 1;
                for(int i = 0; i < m_Main.Children.Count; i++)
                {
                    m_Main.Children.RemoveAt(0);
                }

                switch (menuItem)
                {
                    case MenuItem.Home:
                        Title = "料理人";
                        m_Main.Children.Add(m_Calender);
                        break;
                    case MenuItem.Calender:
                        Title = "カレンダー";
                        break;
                    case MenuItem.School:
                        Title = "スクール";
                        break;
                    case MenuItem.Talk:
                        Title = "トーク";
                        break;
                    case MenuItem.Profile:
                        Title = "プロフィール";
                        break;
                }
        }

        private void CreateLayout()
        {
            //メイン画面を設定します

            m_Menu = new StackLayout();
            m_Layout = new SampleView();
            m_Menu.Children.Add(m_Layout);

            var mainLayout = new SamplePage();
            mainLayout.Content=m_Menu;

            Content = m_Menu;

            SizeChanged += OnPageSizeChanged;
        }
        #endregion

        #region イベント
        private void OnPageSizeChanged(object sender,EventArgs args)
        {
            var width = this.Width;
            var height = this.Height;

            //AbsoluteLayout.SetLayoutFlags(m_Main, AbsoluteLayoutFlags.PositionProportional);
            //AbsoluteLayout.SetLayoutBounds(m_Main, new Rectangle(0d, 0d, width, height));

            //AbsoluteLayout.SetLayoutFlags(m_Menu, AbsoluteLayoutFlags.PositionProportional);
            //AbsoluteLayout.SetLayoutBounds(m_Menu, new Rectangle(0, 1, AbsoluteLayout.AutoSize, AbsoluteLayout.AutoSize));
        }
        #endregion


    }
}
